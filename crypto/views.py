from django.shortcuts import render
import requests, json

key = '0a8afa86-cd6b-4575-9e08-7c99bf313192'
# Create your views here.
def home(request):
    # Grab Crypto News
    api_request = requests.get('https://min-api.cryptocompare.com/data/v2/news/?lang=EN')
    api = json.loads(api_request.content)
    return render(request, 'website/home.html', {'api': api})

def prices(request):
    crypto_list = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30'
    # Api Url
    crypto_price_api = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest?limit=30&CMC_PRO_API_KEY='
    metadata_api = f'https://pro-api.coinmarketcap.com/v1/cryptocurrency/info?id={crypto_list}&CMC_PRO_API_KEY='
    # Add key 
    metadata_api += key
    crypto_price_api += key
    # GET Api Url
    price_request = requests.get(crypto_price_api)
    metadata_request = requests.get(metadata_api)
    # To json
    price = json.loads(price_request.content)
    metadata = json.loads(metadata_request.content)
    if request.method == 'POST':
        quote = request.POST['quote']
        return render(request, 'website/prices.html', {'quote': quote})
    return render(request, 'website/prices.html', {'price': price, 'metadata': metadata})